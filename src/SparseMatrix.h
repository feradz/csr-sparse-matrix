/*
 * SparseMatrix.h
 *
 * CSR sparse matrix
 *
 *  Created on: Feb 5, 2016
 *      Author: Ferad Zyulkyarov
 */

#ifndef SPARSEMATRIX_H_
#define SPARSEMATRIX_H_

#include <stdio.h>


typedef struct
    {
        int dim1, dim2;
        int elemc;
        int *vptr;
        int *vpos;
        double *vval;
    } SparseMatrix, *ptr_SparseMatrix;

extern FILE *OpenFile (char *name, char *attr);

extern void ReadStringFile (FILE *file, char *string, int length);

extern int CreateSparseMatrix (ptr_SparseMatrix spr, int numR, int numC, int numE, int msr);


extern int PrintSparseMatrix (SparseMatrix spr, int CorF);

extern int ProdSparseMatrixVector (SparseMatrix spr, double *vec, double *res);

extern int ProdSymSparseMatrixVector (SparseMatrix spr, double *vec, double *res);

extern void CreateSparseMatrixHB (char *nameFile, ptr_SparseMatrix spr, int FtoC);

#endif /* SPARSEMATRIX_H_ */
