/*
 * SparseMatrix.c
 *
 * Implements sparse matrix in CSR format.
 *
 * Repository with CSR sparse matrices in Rutherford-Boeing format
 * which is a variation of Harwell-Boeing format
 * http://www.cise.ufl.edu/research/sparse/matrices/
 * http://math.nist.gov/MatrixMarket/data/Harwell-Boeing/
 *
 * 5x5 matrix
 * http://people.sc.fsu.edu/~jburkardt/data/hb/5by5_rua.hb
 * http://people.sc.fsu.edu/~jburkardt/c_src/cc_io/cc_io_prb.c
 *
 * Info about HB format
 * http://people.sc.fsu.edu/~jburkardt/data/hb/hb.html
 * http://math.nist.gov/MatrixMarket/formats.html#hb
 *
 * Example
 * https://tedlab.mit.edu/~dr/SVDLIBC/SVD_F_STH.html
 *
 *
 *  Created on: Feb 5, 2016
 *      Author: Ferad Zyulkyarov
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

#include "Vector.h"
#include "SparseMatrix.h"

#define length1 82
#define length2 82

/**
 * Opens a file for reading.
 * @param name the name of the file.
 * @param attr open attribute.
 * @return file handle.
 */
FILE *OpenFile(char *name, char *attr) {
   FILE *fich;
   if ((fich = fopen(name, attr)) == NULL) {
      printf("File %s not exists \n", name);
      exit(1);
   }
   return fich;
}

/**
 * Reads a string from an HB file.
 * @param file the file handle.
 * @param string buffer to read the string in.
 * @param length the size of the buffer.
 */
void ReadStringFile(FILE *file, char *string, int length) {
   char *s = NULL;
   if ((s = fgets(string, length, file)) == NULL) {
      printf("Error reading string \n");
      exit(1);
   }
}

/**
 * Allocate memory for the sparse matrix.
 * @param spr
 * @param numR
 * @param numC
 * @param numE
 * @param msr
 * @return
 */
int CreateSparseMatrix(ptr_SparseMatrix spr, int numR, int numC, int numE, int msr) {
   spr->dim1 = numR;
   spr->dim2 = numC;
   CreateInts(&(spr->vptr), numE + numR + 1);
   *(spr->vptr) = ((msr) ? (numR + 1) : 0);
   spr->vpos = spr->vptr + ((msr) ? 0 : (numR + 1));
   spr->vval = (double*) malloc(sizeof(double) * (numE + (numR + 1) * msr));
   if (spr->vval == NULL) {
      fprintf(stderr, "err: could not allocate vval\n");
      return 1;
   }

   //printf("val %x -> %x\n", spr->vval, spr->vval + numE + (numR + 1) * msr);

   return 0;
}

/**
 * Release the sparse matrix.
 * @param spr
 */
void sparse_free(SparseMatrix *spr) {
   spr->dim1 = -1;
   spr->dim2 = -1;

   free(spr->vptr);
   free(spr->vval);
}

/**
 * Metrix with vector product.
 * @param spr the sparse matrix
 * @param vec the vector
 * @param res the resulting vector
 * @return
 */
int ProdSparseMatrixVector(SparseMatrix spr, double *vec, double *res) {
   int i, j;
   double aux;

   for (i = 0; i < spr.dim1; i++) {
      aux = 0.0;
      for (j = spr.vptr[i]; j < spr.vptr[i + 1]; j++)
         aux += spr.vval[j] * vec[spr.vpos[j]];
      res[i] += aux;
   }

   return 0;
}

/**
 * Multiplication of symmetric matrix with a vector.
 * @param spr the symmetric matrix
 * @param vec the vector
 * @param res the resulting vector
 * @return
 */
int ProdSymSparseMatrixVector(SparseMatrix spr, double *vec, double *res) {
   int i, j, k;
   double aux, val;

   for (i = 0; i < spr.dim1; i++) {
      aux = 0.0;
      for (j = spr.vptr[i]; j < spr.vptr[i + 1]; j++) {
         k = spr.vpos[j];
         val = spr.vval[j];
         aux += val * vec[k];
         if (k != i)
            res[k] += (val * vec[i]);
      }
      res[i] += aux;
   }
   return 0;
}

/**
 * Creates a sparse matrix from an HB file.
 * @param nameFile
 * @param spr
 * @param FtoC
 */
void CreateSparseMatrixHB(char *nameFile, ptr_SparseMatrix spr, int FtoC) {
   FILE *file;
   char string[length1];
   int i, j, k = 0, shft = (FtoC) ? -1 : 0;
   int *vptr = NULL, *vpos = NULL;
   double *vval = NULL;
   int lines[5], dim[4], formats[10];

   file = OpenFile(nameFile, "r");
   ReadStringFile(file, string, length1);  // Line 1: title line
   ReadStringFile(file, string, length1);  // Line 2: TOTCRD, PTRCRD, INDCRD, VALCRD, RHSCRD
   GetIntsFromString(string, lines, 5, 14, 0);
   ReadStringFile(file, string, length1);  // Line 3: PTRFMT, INDFMT, VALFMT, RHSFMT
   GetIntsFromString((string + 14), dim, 4, 14, 0);

   CreateSparseMatrix(spr, dim[0], dim[1], dim[2], 0);
   vptr = spr->vptr;
   vpos = spr->vpos;
   vval = spr->vval;

   ReadStringFile(file, string, length1);  // Line 4: Read formats
   GetFormatsFromString(string, formats, 2, 16);
   GetFormatsFromString((string + 32), (formats + 4), 1 + (lines[4] > 0), 20);

   if (lines[4] > 0)
      ReadStringFile(file, string, length1);

   j = 0;
   for (i = 0; i < lines[1]; i++) {
      ReadStringFile(file, string, length2);
      k = ((dim[0] + 1) - j);
      if (k > formats[0])
         k = formats[0];
      GetIntsFromString(string, (vptr + j), k, formats[1], shft);
      j += formats[0];
   }

   j = 0;
   for (i = 0; i < lines[2]; i++) {
      ReadStringFile(file, string, length2);
      k = (dim[2] - j);
      if (k > formats[2])
         k = formats[2];
      GetIntsFromString(string, (vpos + j), k, formats[3], shft);
      j += formats[2];
   }

   int elemc = 0;
   j = 0;
   for (i = 0; i < lines[3]; i++) {
      ReadStringFile(file, string, length2);
      k = (dim[2] - j);
      if (k > formats[4])
         k = formats[4];
      GetDoublesFromString(string, (vval + j), k, formats[5]);
      elemc += k;

      j += formats[4];
   }

   fclose(file);

   spr->elemc = elemc;
   printf("elemc %i\n", elemc);
}

