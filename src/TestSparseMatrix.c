/*
 ============================================================================
 Name        : TestSparseMatrix.c
 Author      : Ferad Zyulkyarov
 Version     :
 Copyright   : 
 Description : Sparse matrix in CSR format. The input matrix should be in
               Harwell-Boeing format.


 Repository with sparse matrices
 -------------------------------
 http://www.cise.ufl.edu/research/sparse/matrices/

 There are many matrices in the repository in different format.
 Those which are Rutherford-Boeing format are compatible with this code.
 Rutherford-Boeing is compatible with Harwell-Boeing format.

 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "SparseMatrix.h"

int main(int argc, char* argv[]) {
   if (argc != 2) {
      fprintf(stderr, "Usage: TestSparseMatrix <HB file>\n");
      return EXIT_FAILURE;
   }

   SparseMatrix mat;
   CreateSparseMatrixHB (argv[1], &mat, 1);


   printf("Done\n");
   return EXIT_SUCCESS;
}
